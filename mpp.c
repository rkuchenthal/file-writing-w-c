#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])

{
    if (argc < 2)
        {
            
            printf("Please supply an input and output file\n");
            exit(2);
            
        }

    FILE *f;
    
    f = fopen(argv[1], "r");
    

    if (!f)
        {
            
            
            printf("Can't open %s for reading\n", argv[1]);
            
            exit(1);
            
        }
    

    FILE *fileOut;
    

    if (argc == 3)
    {
        
        fileOut = fopen(argv[2], "w");
        
        if (!fileOut)
            {
                
                printf("Can't open %s for writing\n", argv[2]);
                exit(1);
                
            }
            
    }

    char fileLine[100];
    
    while(fgets(fileLine, 100, f) != NULL)
        {
            
          for(int i = 0; i < strlen(fileLine); i++)
             {

                if(fileLine[i] == '/')
                    {
                    
                     fileLine[i] = '\n';
                     fileLine[i + 1] = '\0';
                
                    }
            
                 else if (fileLine[i] == '#')
                    {
                      FILE *fp;
                
                 char filename[50];
                
                 sscanf(fileLine, "#include %s", filename);
                
                 fp = fopen(filename, "r");
    
                 if(!fp)
                    {
                        printf("Can't open %s for reading\n", argv[1]);
                        exit(1);
                    }
                 char fileLine2[100];
    
                 while(fgets(fileLine2, 100, fp) != NULL)
                        {
                            
                          fprintf(fileOut, "%s", fileLine2);
                        
                        }
    
                 if(fileLine[i] == '#')
                        {
                            fileLine[i] = '\n';
                            
                            fileLine[i + 1] = '\0';
                         }
                         
                    fclose(fp);
                    
               }
        }
        
    if (argc == 3)
        {
           fprintf(fileOut, "%s", fileLine);
        }
    
    else
        {
            
           printf("%s", fileLine);
        
         }

    }

}